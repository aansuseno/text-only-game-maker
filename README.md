# Text Adventure Game Maker - CodeIgniter 4

## Apa sih?

Adalah aplikasi berbasis web yang dibuat menggunakan [CodeIgniter 4](https://github.com/codeigniter4/CodeIgniter4). Cara bermainnya yaitu user akan memilih cerita yang dibuat oleh user lain, lalu user akan menentukan langkah berikutnya berdasarkan cerita yang sekarang.

## Pemasangan di XAMPP

1. Pertama clone dahulu repositori ini di dalam folder htdocs. `git clone https://gitlab.com/aansuseno/text-only-game-maker.git`
2. Buat database baru.
3. Rename file *env* menjadi *.env*
4. Di file *.env*, ubah nama databasenya dan hilangkan tanda `#`, sesuaikan dengan username dan password. Biasanya username *root*. Password kosongin aja.
5. Kembali ke cmd, lakukan perintah: `php spark migrate` untuk menjalankan migrasi database.
6. Terakhir, jalankan perintah `php spark serve` untuk menjalankan aplikasi. Sekarang buka http://127.0.0.1:8080 di browser. Selesai.

## Lisensi
Diperbolehkan untuk mencopy kode dan menclone repositori, dengan syarat mencantumkan sumber. Terimakasih.
