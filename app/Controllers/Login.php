<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\User;

class Login extends BaseController
{
	public function __construct() {
		$this->muser = new User();
	}

    public function index()
    {
        return view('login');
    }

	public function daftar()
	{
		return view('daftar');
	}

	public function daftarProses()
	{
		if (count($this->muser->where('username', $_POST['username'])->findAll()) > 0) {
			session()->setFlashdata('pesan', 'Username sudah digunakan');
			return redirect()->to('daftar');
		}
		$data = [
			'username' => $_POST['username'],
			'password' => md5($_POST['password'])
		];
		$this->muser->insert($data);
		session()->setFlashdata('pesan', 'Mendaftar berhasil, silakan masuk');
		return redirect()->to('login');
	}

	public function loginProses()
	{
		$data = [
			'username' => $_POST['username'],
			'password' => md5($_POST['password']),
		];
		if (count($this->muser->where($data)->findAll()) == 0) {
			session()->setFlashdata('pesan', 'Password atau username salah.');
			return redirect()->to('login');
		}
		$user = $this->muser->where($data)->first();
		session()->login = 'oke';
		session()->id_user = $user['id'];
		return redirect()->to('/');
	}
}
