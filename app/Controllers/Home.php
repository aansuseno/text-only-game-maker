<?php

namespace App\Controllers;
use App\Models\Cerita;
use App\Models\Halaman;
use App\Models\User;
use App\Models\Alur;
use App\Models\Up;

class Home extends BaseController
{
	public function __construct() {
		$this->mcerita = new Cerita();
		$this->mhalaman = new Halaman();
		$this->muser = new User();
		$this->mup = new Up();
		$this->malur = new Alur();
	}
    public function index($halaman = 0)
    {
		$banyakData = 10;
		$cari = "";
		if (isset($_POST['cari'])) {
			$cari = $_POST['cari'];
		}
		$dimulaiDari = $halaman * $banyakData;
		$cerita = $this->mcerita
			->where(['tayangkah' => 1])
			->groupStart()
			->like('judul', $cari)
			->orLike('deskripsi', $cari)
			->orLike('genre', $cari)
			->groupEnd()
			->orderBy('updated_at', 'DESC')
			->findAll($banyakData, $dimulaiDari);
		$total = count($this->mcerita
			->where(['tayangkah' => 1])
			->groupStart()
			->like('judul', $cari)
			->orLike('deskripsi', $cari)
			->orLike('genre', $cari)
			->groupEnd()
			->findAll());
		$halaman_max = (int)($total/$banyakData);
		$penulis = [];
		$suka = [];

		if ($total == 0) {
			session()->setFlashdata('pesan', 'Tidak ada cerita.');
		}

		for ($i=0; $i < count($cerita); $i++) { 
			$penulis_baru = $this->muser->where(['id' => $cerita[$i]['id_user']])->first();
			$suka_baru = count($this->mup->where(['id_cerita' => $cerita[$i]['id']])->findAll());
			array_push($penulis, $penulis_baru);
			array_push($suka, $suka_baru);
		}
		
		$dataTampil = [
			'cerita' => $cerita,
			'penulis' => $penulis,
			'suka' => $suka,
			'halaman' => $halaman,
			'halaman_max' => $halaman_max,
			'cari' => $cari,
		];
        return view('user/index', $dataTampil);
    }

	public function keluar()
	{
		session()->destroy();
		return redirect()->to('login');
	}

	// biar gak ada yang menghack milik orang lain
	public function cekidhalaman($id)
	{
		$halaman = $this->mhalaman->where(['id' => $id])->first();
		if ($halaman != null) {
			return $this->cekidcerita($halaman['id_cerita']);
		} else {
			return false;
		}
		
	}

	public function cekidcerita($id)
	{
		$cerita = $this->mcerita->where(['id' => $id])->first();
		// biar ngga bisa akses punya orang lain
		if ($cerita == null) {
			return false;
		} else if ($cerita['id_user'] != $_SESSION['id_user']) {
			return false;
		} else {
			return true;
		}
	}

	public function ceritaku()
	{
		$dataTampil = [
			'cerita' => $this->mcerita->where(['id_user' => session()->id_user])->orderBy('id', 'DESC')->findAll(),
		];
		return view('user/ceritaku', $dataTampil);
	}

	public function tambah()
	{
		return view('user/tambahCerita');
	}

	public function tambahHalaman($id_halaman)
	{
		if (!$this->cekidhalaman($id_halaman)) return redirect()->to('/');
		$berindukkah = false;
		$sebelum = 0;
		$adakah = $this->malur->where(['id_halaman_tujuan' => $id_halaman])->first();
		if ($adakah != null) {
			$berindukkah = true;
			$sebelum = $adakah['id_halaman_asal'];
		}
		$halaman = $this->mhalaman->where(['id' => $id_halaman])->first();
		$cerita = $this->mcerita->where(['id' => $halaman['id_cerita']])->first();

		$dataTampil = [
			'halaman' => $halaman,
			'cerita' => $cerita,
			'pilihan' => $this->malur->where(['id_halaman_asal' => $halaman['id']])->findAll(),
			'berindukkah' => $berindukkah,
			'sebelum' => $sebelum,
		];
		return view('user/tambahHalaman', $dataTampil);
	}

	public function detailCeritaku($id_cerita)
	{
		if(!$this->cekidcerita($id_cerita)) return redirect()->to('/');
		$cerita = $this->mcerita->where(['id' => $id_cerita])->first();
		$halaman = $this->mhalaman->where(['id_cerita' => $cerita['id']])->first();

		$dataTampil = [
			'cerita' => $cerita,
			'halaman' => $halaman,
			'pilihan' => count($this->malur->where(['id_halaman_asal' => $halaman['id']])->findAll()),
		];
		return view('user/detail-ceritaku', $dataTampil);
	}

	public function tambahCeritaProses()
	{
		$dataSimpan = [
			'judul' => $_POST['judul'],
			'deskripsi' => $_POST['deskripsi'],
			'genre' => $_POST['genre'],
			'id_user' => $_SESSION['id_user']
		];
		$this->mcerita->insert($dataSimpan);
		$cerita = $this->mcerita->orderBy('id', 'DESC')->first();

		$dataSementara = [
			'judul' => 'Judul Palsu',
			'isi' => 'Isi sementara',
			'id_cerita' => $cerita['id'],
		];
		$this->mhalaman->insert($dataSementara);
		$halaman = $this->mhalaman->orderBy('id', 'DESC')->first();
		session()->setFlashdata('pesan', 'Berhasil membuat cerita baru.');
		return redirect()->to("buatHalaman/{$halaman['id']}");
	}

	public function gantiIsiHalaman()
	{
		if(!$this->cekidhalaman($_POST['id'])) return redirect()->to('/');
		$halaman = $this->mhalaman->where(['id' => $_POST['id']])->first();
		$cerita = $this->mcerita->where(['id' => $halaman['id_cerita']])->first();

		$dataUpdate = [
			'judul' => $_POST['judul'],
			'isi' => $_POST['isi'],
		];
		$this->mhalaman->update($_POST['id'], $dataUpdate);
		session()->setFlashdata('pesan', 'Perubahan berhasil disimpan.');
		return redirect()->to("buatHalaman/{$halaman['id']}");
	}

	public function tayangkanCerita()
	{
		if(!$this->cekidcerita($_POST['id'])) return redirect()->to('/');
		
		$dataUpdate = [
			'tayangkah' => 1,
		];
		$this->mcerita->update($_POST['id'], $dataUpdate);
		session()->setFlashdata('pesan', 'Cerita berhasil dipublikasikan.');
		return redirect()->to("/detailCeritaku/{$_POST['id']}");
	}

	public function sembunyikanCerita()
	{
		if(!$this->cekidcerita($_POST['id'])) return redirect()->to('/');
		
		$dataUpdate = [
			'tayangkah' => 0,
		];
		$this->mcerita->update($_POST['id'], $dataUpdate);
		session()->setFlashdata('pesan', 'Cerita berhasil disembunyikan.');
		return redirect()->to("/detailCeritaku/{$_POST['id']}");
	}
	
	public function simpanAlur()
	{
		if(!$this->cekidhalaman($_POST['id_halaman_asal'])) return redirect()->to('/');

		$halaman = $this->mhalaman->where(['id' => $_POST['id_halaman_asal']])->first();
		$dataSementara = [
			'judul' => $_POST['text'],
			'isi' => 'Isi sementara',
			'id_cerita' => $halaman['id_cerita'],
		];
		$this->mhalaman->insert($dataSementara);

		$halamanBaru = $this->mhalaman->orderBy('id', 'DESC')->first();
		$dataSimpan = [
			'id_halaman_asal' => $_POST['id_halaman_asal'],
			'id_halaman_tujuan' => $halamanBaru['id'],
			'text' => $_POST['text']
		];
		$this->malur->insert($dataSimpan);
		session()->setFlashdata('pesan', 'Pilihan baru berhasil dibuat.');
		return redirect()->to("/buatHalaman/{$_POST['id_halaman_asal']}");
	}

	public function hapusAlur()
	{
		$alur  = $this->malur->where(['id' => $_POST['id']])->first();
		$asal = $alur['id_halaman_asal'];
		if (!$this->cekidhalaman($alur['id_halaman_asal'])) return redirect()->to('/');
		
		$this->mhalaman->delete(['id' => $alur['id_halaman_tujuan']]);
		$this->malur->delete(['id' => $alur['id']]);
		session()->setFlashdata('pesan', 'Berhasil menghapus pilihan.');
		return redirect()->to("/buatHalaman/{$asal}");
	}

	public function editAlur()
	{
		$alur  = $this->malur->where(['id' => $_POST['id']])->first();
		if (!$this->cekidhalaman($alur['id_halaman_asal'])) return redirect()->to('/');

		$this->malur->update($alur['id'], ['text' => $_POST['text']]);
		session()->setFlashdata('pesan', 'Teks pilihan berhasil diganti.');
		return redirect()->to("/buatHalaman/{$alur['id_halaman_asal']}");
	}

	public function ambilPilihan($id_halaman)
	{
		if (!$this->cekidhalaman($id_halaman)) return redirect()->to('/');
		$alur = $this->malur->where(['id_halaman_asal' => $id_halaman])->findAll();
		$pilihan = [];
		$jumlah_pilihan = [];
		
		for ($i=0; $i < count($alur); $i++) { 
			$pilihan_baru = $this->mhalaman->where(['id' => $alur[$i]['id_halaman_tujuan']])->first();
			$jumlah_pilihan_baru = $this->malur->where(['id_halaman_asal' => $pilihan_baru['id']])->findAll();
			if ($jumlah_pilihan_baru == null) {
				array_push($jumlah_pilihan, 0);
			} else {
				array_push($jumlah_pilihan, count($jumlah_pilihan_baru));
			}
			array_push($pilihan, $pilihan_baru);
		}

		$dataTampil = [
			'pilihan' => $pilihan,
			'jumlah' => $jumlah_pilihan,
		];
		return view('user/pilihan', $dataTampil);
	}

	public function gantipw()
	{
		return view('user/gantipw');
	}

	public function gantipwProses()
	{
		if ($_POST['password'] == $_POST['konfirmasi']) {
			$this->muser->update(session()->id_user, ['password' => md5($_POST['password'])]);
			session()->setFlashdata('pesan', 'Berhasil mengganti password');
		} else {
			session()->setFlashdata('pesan', 'Gagal mengganti password');
		}
		return redirect()->to('/gantipw');
	}

	public function hapusCerita()
	{
		if (!$this->cekidcerita($_POST['id'])) return redirect()->to('/');
		$this->mcerita->delete(['id' => $_POST['id']]);
		$this->mhalaman->delete(['id_cerita' => $_POST['id']]);
		$this->mup->delete(['id_cerita' => $_POST['id']]);

		session()->setFlashdata('pesan', 'Cerita berhasil dihapus.');
		return redirect()->to('/ceritaku');
	}

	public function profil($username)
    {
		$user = $this->muser->where(['username' => $username])->first();
		$dataTampil = [
			'cerita' => $this->mcerita->where(['id_user' => $user['id'], 'tayangkah' => 1])->orderBy('id', 'DESC')->findAll(),
			'user' => $user,
		];
		return view('user/profil', $dataTampil);
    }
}
