<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\Baca;
use App\Models\User;
use App\Models\Halaman;
use App\Models\Cerita;
use App\Models\Alur;
use App\Models\Up;

class Membaca extends BaseController
{
	public function __construct() {
		$this->mbaca = new Baca();
		$this->muser = new User();
		$this->mhalaman = new Halaman();
		$this->mcerita = new Cerita();
		$this->malur = new Alur();
		$this->mup = new Up();
	}
    public function index($id_cerita)
    {
        $membacakah = $this->mbaca->where(['id_cerita' => $id_cerita, 'id_user' => session()->id_user])->first();
		$halaman = 0;

		if($membacakah == null) {
			$halaman_pertama = $this->mhalaman->where(['id_cerita' => $id_cerita])->first();
			$dataTambah = [
				'id_user' => session()->id_user,
				'id_cerita' => $id_cerita,
				'id_halaman' => $halaman_pertama['id'],
			];
			$this->mbaca->insert($dataTambah);
			$halaman = $halaman_pertama['id'];
		} else {
			$halaman = $membacakah['id_halaman'];
		}

		$halaman_b = $this->mhalaman->where(['id' => $halaman])->first();
		$pilihan = $this->malur->where(['id_halaman_asal' => $halaman])->orderBy('text')->findAll();
		$suka = false;
		if (count($this->mup->where(['id_user' => session()->id_user, 'id_cerita' => $id_cerita])->findAll()) > 0) {
			$suka = true;
		}
		$dataTampil = [
			'halaman' => $halaman_b,
			'pilihan' => $pilihan,
			'suka' => $suka,
		];
		return view('user/membaca', $dataTampil);
    }

	public function lanjutBaca($id)
	{
		$baca = $this->mbaca->where(['id_user' => session()->id_user, 'id_halaman' => $_POST['id_awal']])->first();
		$baru = $this->malur->where(['id_halaman_asal' => $baca['id_halaman']])->findAll();

		$hehe = true;
		for ($i=0; $i < count($baru); $i++) { 
			if ($baru[$i]['id_halaman_tujuan'] == $id) {
				$hehe = false;
			}
		}
		if ($hehe) return redirect()->to('/');
		$this->mbaca->update($baca['id'], ['id_halaman' => $id]);

		return redirect()->to("/baca/{$baca['id_cerita']}");
	}

	public function reset()
	{
		$dataSekarang = $this->mbaca->where(['id_cerita' => $_POST['id_cerita'], 'id_user' => session()->id_user])->first();
		$halaman_pertama = $this->mhalaman->where(['id_cerita' => $_POST['id_cerita']])->first();
		$this->mbaca->update(['id' => $dataSekarang['id']], ['id_halaman' => $halaman_pertama['id']]);
		return redirect()->to('/baca/'.$_POST['id_cerita']);
	}

	public function dibaca()
	{
		$dibaca = $this->mbaca->where(['id_user' => session()->id_user])->orderBy('updated_at', 'DESC')->findAll();
		$cerita = [];
		$penulis = [];

		for ($i=0; $i < count($dibaca); $i++) {
			$cerita_baru = $this->mcerita->where(['tayangkah' => 1, 'id' => $dibaca[$i]['id_cerita']])->first();
			if($cerita_baru == null) continue;
			$penulis_baru = $this->muser->where(['id' => $cerita_baru['id_user']])->first();
			array_push($penulis, $penulis_baru);
			array_push($cerita, $cerita_baru);
		}
		$dataTampil = [
			'cerita' => $cerita,
			'penulis' => $penulis,
		];
        return view('user/dibaca', $dataTampil);
	}

	public function hapusBaca()
	{
		$kondisi = [
			'id_cerita' => $_POST['id_cerita'],
			'id_user' => session()->id_user
		];
		$baca = $this->mbaca->where($kondisi)->first();
		$this->mbaca->delete(['id' => $baca['id']]);
		session()->setFlashdata('pesan', 'Riwayat berhasil dihapus.');
		return redirect()->to('/membaca');
	}

	public function suka($id_cerita)
	{
		$kondisi = [
			'id_user' => session()->id_user,
			'id_cerita' => $id_cerita
		];
		$banyaknya = $this->mup->where($kondisi)->first();
		if ($banyaknya == null) {
			$this->mup->insert($kondisi);
			session()->setFlashdata('pesan', 'Terimakasih sudah menambah semangat author.');
		} else {
			session()->setFlashdata('pesan', 'Berhasil membatalkan suka.');
			$this->mup->delete(['id' => $banyaknya['id']]);
		}
		return redirect()->to("/baca/{$id_cerita}");
	}

	public function editCerita($id)
	{
		$dataTampil = [
			'cerita' => $this->mcerita->where(['id' => $id])->first()
		];
		return view('user/editCerita', $dataTampil);
	}

	public function editCeritaProses($id)
	{
		$this->mcerita->update(['id' => $id], $_POST);
		session()->setFlashdata('pesan', 'Cerita berhasil diedti.');
		return redirect()->to('/detailCeritaku/'.$id);
	}
}
