<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Cerita extends Migration
{
    public function up()
    {
		$this->forge->addField([
			'id'          => [
				'type'           => 'INT',
				'constraint'     => 9,
				'unsigned'       => true,
				'auto_increment' => true,
			],
			'id_user'          => [
				'type'           => 'INT',
				'constraint'     => 9,
				'unsigned'       => true,
			],
			'judul'       => [
				'type'       => 'VARCHAR',
				'constraint' => '255',
			],
			'deskripsi'			=> [
				'type'			=> 'TEXT',
			],
			'genre'			=> [
				'type'			=> 'TEXT',
			],
			'tayangkah'		=> [
				'type'		=> 'INT',
				'constraint'=> 2,
				'default'	=> 0,
			],
			'created_at'	=> [
				'type'		=> 'DATETIME'
			],
			'updated_at'	=> [
				'type'		=> 'DATETIME'
			]
		]);
		$this->forge->addKey('id', true);
		$this->forge->addForeignKey('id_user', 'user', 'id', 'cascade', 'cascade');
		$this->forge->createTable('cerita');
	}

	public function down()
	{
		$this->forge->dropTable('cerita');
    }
}
