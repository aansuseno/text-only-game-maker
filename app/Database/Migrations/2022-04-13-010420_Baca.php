<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Baca extends Migration
{
    public function up()
    {
		$this->forge->addField([
			'id'          => [
				'type'           => 'INT',
				'constraint'     => 9,
				'unsigned'       => true,
				'auto_increment' => true,
			],
			'id_user'          => [
				'type'           => 'INT',
				'constraint'     => 9,
				'unsigned'       => true,
			],
			'id_cerita'          => [
				'type'           => 'INT',
				'constraint'     => 9,
				'unsigned'       => true,
			],
			'id_halaman'          => [
				'type'           => 'INT',
				'constraint'     => 9,
				'unsigned'       => true,
			],
			'created_at'	=> [
				'type'		=> 'DATETIME'
			],
			'updated_at'	=> [
				'type'		=> 'DATETIME'
			]
		]);
		$this->forge->addKey('id', true);
		$this->forge->addForeignKey('id_user', 'user', 'id', 'cascade', 'cascade');
		$this->forge->addForeignKey('id_cerita', 'cerita', 'id', 'cascade', 'cascade');
		$this->forge->addForeignKey('id_halaman', 'halaman', 'id', 'cascade', 'cascade');
		$this->forge->createTable('baca');
	}

	public function down()
	{
		$this->forge->dropTable('baca');
    }
}
