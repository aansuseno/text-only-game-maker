<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Alur extends Migration
{
    public function up()
    {
		$this->forge->addField([
			'id'          => [
				'type'           => 'INT',
				'constraint'     => 9,
				'unsigned'       => true,
				'auto_increment' => true,
			],
			'id_halaman_asal'          => [
				'type'           => 'INT',
				'constraint'     => 9,
				'unsigned'       => true,
			],
			'id_halaman_tujuan'          => [
				'type'           => 'INT',
				'constraint'     => 9,
				'unsigned'       => true,
			],
			'text'       => [
				'type'       => 'VARCHAR',
				'constraint' => '255',
			],
			'created_at'	=> [
				'type'		=> 'DATETIME'
			],
			'updated_at'	=> [
				'type'		=> 'DATETIME'
			]
		]);
		$this->forge->addKey('id', true);
		$this->forge->addForeignKey('id_halaman_asal', 'halaman', 'id', 'cascade', 'cascade');
		$this->forge->createTable('alur');
	}

	public function down()
	{
		$this->forge->dropTable('alur');
    }
}
