<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Halaman extends Migration
{
    public function up()
    {
		$this->forge->addField([
			'id'          => [
				'type'           => 'INT',
				'constraint'     => 9,
				'unsigned'       => true,
				'auto_increment' => true,
			],
			'id_cerita'          => [
				'type'           => 'INT',
				'constraint'     => 9,
				'unsigned'       => true,
			],
			'judul'       => [
				'type'       => 'VARCHAR',
				'constraint' => '255',
			],
			'isi'			=> [
				'type'			=> 'TEXT',
			],
			'created_at'	=> [
				'type'		=> 'DATETIME'
			],
			'updated_at'	=> [
				'type'		=> 'DATETIME'
			]
		]);
		$this->forge->addKey('id', true);
		$this->forge->addForeignKey('id_cerita', 'cerita', 'id', 'cascade', 'cascade');
		$this->forge->createTable('halaman');
	}

	public function down()
	{
		$this->forge->dropTable('halaman');
    }
}
