<?= $this->extend('user/template') ?>

<?= $this->section('konten') ?>
<p><?= $halaman['isi'] ?></p>
<div style="display:flex; flex-wrap: wrap;justify-content: space-evenly">
	<?php if (count($pilihan) > 0) { ?>
		<?php for ($i=0; $i < count($pilihan); $i++) { ?>
			<form action="/baca/<?= $pilihan[$i]['id_halaman_tujuan']?>" method="post">
				<?= csrf_field() ?>
				<input type="hidden" name="id_awal" value="<?= $halaman['id']?>">
				<button type="submit" class="pilihan""><?= $pilihan[$i]['text']?></button>
			</form>
		<?php } ?>
	<?php } else { ?>
		<button class="pilihan" style="background: #7733dd; color: #fff;" onclick="window.location='/suka/<?= $halaman['id_cerita']?>'"><?php if($suka) echo 'BATAL '; ?>SUKA</button>
		<button class="pilihan bg-bahaya" onclick="reset()">RESET</button>
	<?php } ?>
</div>

<div class="bg-popup" id="popup-reset" onclick="batal('popup-reset')">
	<div class="kartu-popup" onclick="event.stopPropagation()">
		<h3>Peringatan</h3>
		<form action="/reset" method="post">
			<?= csrf_field() ?>
			<input type="hidden" name="id_cerita" value="<?= $halaman['id_cerita']?>">
			<p>Cerita ini akan diulang dari awal, apa Anda yakin?</p>
			<input type="submit" class="tmbl bg-bahaya" value="RESET">
			<span onclick="batal('popup-reset')" class="tmbl" style="background: #888888;box-shadow: none; float: right">BATAL</span>
		</form>
	</div>
</div>

<script>
	function batal(id) {
		document.getElementById(id).style.display = 'none'
	}
	function reset() {
		document.getElementById('popup-reset').style.display = 'block'
	}
</script>
<?= $this->endSection() ?>