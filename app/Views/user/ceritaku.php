<?= $this->extend('user/template') ?>

<?= $this->section('konten') ?>
<a href="/tambah" class="tmbl">Buat Cerita</a>
<br><br>
<?php for ($i=0; $i < count($cerita); $i++) { ?>
	<div class="cerita <?= ($cerita[$i]['tayangkah'] == 0) ? 'draft' :'' ?>" onclick="window.location='/detailCeritaku/<?= $cerita[$i]['id']?>'">
		<p><b><?= $cerita[$i]['judul']?></b></p>
		<hr>
		<p><?= $cerita[$i]['deskripsi']?></p>
	</div>
<?php } ?>
<?= $this->endSection() ?>