<?= $this->extend('user/template') ?>

<?= $this->section('konten') ?>
<h3><?= $cerita['judul'] ?></h3>

<?php if ($cerita['tayangkah'] == 0) { ?>
	<button class="tmbl" style="background: #7777dd" onclick="publikkan()">Publikkan</button>
<?php } else { ?>
	<button class="tmbl" onclick="sembunyikan()">Sembunyikan</button>
<?php } ?>
<button class="tmbl bg-peringatan" onclick="window.location='/editCeritaku/<?=$cerita['id']?>'">Edit</button>
<button class="tmbl bg-bahaya" onclick="hapus()">Hapus</button>
<hr>
<div style="overflow: auto;">
	<ol>
		<li>
			<?= $halaman['judul']?>
			<span class="tmbl-kecil bg-peringatan" onclick="(window.location='/buatHalaman/<?=$halaman['id']?>')">edit</span>
			<?php if($pilihan >= 1) { ?>
				<span class="tmbl-kecil bg-oke" onclick="tampilPilihan(<?=$halaman['id']?>)">--v--</span>
				<span id="det-<?=$halaman['id']?>"></span>
			<?php } ?>
		</li>
	</ol>
</div>

<?php if ($cerita['tayangkah'] == 0) { ?>
	<div class="bg-popup" id="popup-publik" onclick="batal('popup-publik')">
		<div class="kartu-popup" onclick="event.stopPropagation()">
			<h3><center>PERINGATAN !</center></h3>
			<form action="/tayangkanCerita" method="post">
				<?= csrf_field() ?>
				<input type="hidden" name="id" value="<?= $cerita['id']?>">
				<p>Apa anda yakin ingin mempublikkan? Siapa saja bisa melihat.</p>
				<input type="submit" class="tmbl" style="background: #7777dd" value="YA">
				<span onclick="batal('popup-publik')" class="tmbl" style="background: #888888;box-shadow: none; float: right">BATAL</span>
			</form>
		</div>
	</div>
<?php } else { ?>
	<div class="bg-popup" id="popup-sembunyikan" onclick="batal('popup-sembunyikan')">
		<div class="kartu-popup" onclick="event.stopPropagation()">
			<h3><center>PERINGATAN !</center></h3>
			<form action="/sembunyikanCerita" method="post">
				<?= csrf_field() ?>
				<input type="hidden" name="id" value="<?= $cerita['id'] ?>">
				<p>Apa anda yakin ingin menyembunyikan cerita ini? Tidak ada yang bisa melihat.</p>
				<input type="submit" class="tmbl" value="YA">
				<span onclick="batal('popup-sembunyikan')" class="tmbl" style="background: #888888;box-shadow: none; float: right">BATAL</span>
			</form>
		</div>
	</div>
<?php } ?>

<div class="bg-popup" id="popup-hapus" onclick="batal('popup-hapus')">
	<div class="kartu-popup" onclick="event.stopPropagation()">
		<h3><center>PERINGATAN !</center></h3>
		<form action="/hapus-cerita" method="post">
			<?= csrf_field() ?>
			<input type="hidden" name="id" value="<?=$halaman['id_cerita']?>">
			<p>Apa anda yakin ingin menghapus cerita secara permanen?</p>
			<input type="submit" class="tmbl bg-bahaya" value="HAPUS">
			<span onclick="batal('popup-hapus')" class="tmbl" style="background: #888888;box-shadow: none; float: right">BATAL</span>
		</form>
	</div>
</div>

<script>
	function publikkan() {
		document.getElementById('popup-publik').style.display = 'block'
	}
	function sembunyikan() {
		document.getElementById('popup-sembunyikan').style.display = 'block'
	}
	function hapus() {
		document.getElementById('popup-hapus').style.display = 'block'
	}
	function batal(id) {
		document.getElementById(id).style.display = 'none'
	}
	function tampilPilihan(id) {
		const h = document.getElementById('det-'+id)
		if (h.innerHTML == '') {
			const xhttp = new XMLHttpRequest()
			xhttp.onreadystatechange = function () {
				if (this.status == 200) {
					h.innerHTML = this.responseText
				}
			}
			xhttp.open('GET', '/ambilPilihan/'+id, true)
			xhttp.send()
		} else {
			h.innerHTML = ''
		}
	}
</script>
<?= $this->endSection() ?>