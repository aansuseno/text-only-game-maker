<?= $this->extend('user/template') ?>

<?= $this->section('konten') ?>
<h3><?= $cerita['judul'] ?></h3>

<form action="/editCeritaProses/<?=$cerita['id']?>" method="post" class="tambah-cerita">
	<?= csrf_field() ?>
	<input type="text" name="judul" placeholder="Judul" max-length="100" title="Inputkan judul cerita di sini." required autofocus autocomplete="off" value="<?=$cerita['judul']?>"><br>
	<textarea name="deskripsi" rows="10" placeholder="deskripsi atau sinopsis cerita"><?=$cerita['deskripsi']?></textarea><br>
	<input type="text" name="genre" value="<?=$cerita['genre']?>" autocomplete="off" title="Isi genre seperti fiksi ilmiah, komedi, dll. Pisahkan dengan tanda koma." placeholder="Genre"><br>
	<input type="submit" class="tmbl tmblbuat" value="UPDATE">
</form>
<?= $this->endSection() ?>