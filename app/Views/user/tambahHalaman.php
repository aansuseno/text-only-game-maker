<?= $this->extend('user/template') ?>

<?= $this->section('konten') ?>
<h3><?= $cerita['judul'] ?> - Tambah Halaman</h3>

<?php if($berindukkah) : ?>
	<a href="/buatHalaman/<?=$sebelum?>" class="tmbl">halaman sebelumnya</a>
<?php endif ?>

<form action="/gantiIsiHalaman" style="margin-top: 20px;" method="post" class="tambah-cerita">
	<?= csrf_field() ?>
	<input type="hidden" name="id" value="<?= $halaman['id'] ?>">
	<input type="text" name="judul" placeholder="Judul halaman" title="Inputkan judul halaman disini di sini, digunakan untuk memudahkan anda menemukan halaman." value="<?= $halaman['judul'] ?>" required autofocus autocomplete="off"><br>
	<textarea name="isi" rows="10" placeholder="isi cerita"><?= $halaman['isi'] ?></textarea><br>
	<input type="submit" class="tmbl tmblbuat" value="SIMPAN">
</form>
<br>
<button class="tmbl" onclick="buatKartu()">Buat Pilihan</button>
<br><br>
<p>Pilihan:</p>
<div style="display:flex; flex-wrap: wrap;">
	<?php for ($i=0; $i < count($pilihan); $i++) { ?>
		<div class="pilihan"><?= $pilihan[$i]['text']?>
			<span class="tmbl-kecil bg-oke" onclick="window.location='/buatHalaman/<?= $pilihan[$i]['id_halaman_tujuan']?>'">tulis</span>
			<span class="tmbl-kecil bg-peringatan" onclick="edit(<?= $pilihan[$i]['id']?>, '<?= $pilihan[$i]['text']?>')">edit</span>
			<span class="tmbl-kecil bg-bahaya" onclick="hapus(<?= $pilihan[$i]['id']?>)">hapus</span>
		</div>
	<?php } ?>
</div>

<div class="bg-popup" id="popup" onclick="batal('popup')">
	<div class="kartu-popup" onclick="event.stopPropagation()">
		<h3>Pilihan</h3>
		<form action="/simpanAlur" method="post">
			<?= csrf_field() ?>
			<input type="hidden" name="id_halaman_asal" value="<?= $halaman['id'] ?>">
			<p>Teks:</p>
			<input type="text" style="width: 100%;padding: 7px;" placeholder="teks tombol" title="masukkan tulisan di tombol, untuk memudahkan pembaca" autocomplete="off" required max-length="255" name="text" id=""><b></b><br><br>
			<input type="submit" class="tmbl" style="background: #ff88ff;box-shadow: none" value="TAMBAH">
			<span onclick="batal('popup')" class="tmbl" style="background: #888888;box-shadow: none; float: right">BATAL</span>
		</form>
	</div>
</div>

<div class="bg-popup" id="popup-edit" onclick="batal('popup-edit')">
	<div class="kartu-popup" onclick="event.stopPropagation()">
		<h3>Pilihan</h3>
		<form action="/editAlur" method="post">
			<?= csrf_field() ?>
			<input type="hidden" name="id" id="id_edit">
			<p>Edit:</p>
			<input type="text" autocomplete="off" style="width: 100%;padding: 7px;" placeholder="teks tombol" title="masukkan tulisan di tombol, untuk memudahkan pembaca" id="text_edit" required max-length="255" name="text"><b></b><br><br>
			<input type="submit" class="tmbl bg-peringatan" value="EDIT">
			<span onclick="batal('popup-edit')" class="tmbl" style="background: #888888;box-shadow: none; float: right">BATAL</span>
		</form>
	</div>
</div>

<div class="bg-popup" id="popup-hapus" onclick="batal('popup-hapus')">
	<div class="kartu-popup" onclick="event.stopPropagation()">
		<h3>Pilihan</h3>
		<form action="/hapusAlur" method="post">
			<?= csrf_field() ?>
			<input type="hidden" name="id" id="id_hapus">
			<p>Apa anda yakin ingin mengahapus secara permanen?</p>
			<input type="submit" class="tmbl bg-bahaya" value="HAPUS">
			<span onclick="batal('popup-hapus')" class="tmbl" style="background: #888888;box-shadow: none; float: right">BATAL</span>
		</form>
	</div>
</div>

<script>
	function buatKartu() {
		document.getElementById('popup').style.display = 'block'
	}
	function batal(id) {
		document.getElementById(id).style.display = 'none'
	}
	function hapus(id) {
		document.getElementById('popup-hapus').style.display = 'block'
		document.getElementById('id_hapus').value = id
	}
	function edit(id, text) {
		document.getElementById('popup-edit').style.display = 'block'
		document.getElementById('id_edit').value = id
		document.getElementById('text_edit').value = text
	}
</script>
<?= $this->endSection() ?>