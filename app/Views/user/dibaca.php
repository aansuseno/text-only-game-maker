<?= $this->extend('user/template') ?>

<?= $this->section('konten') ?>
<?php for ($i=0; $i < count($cerita); $i++) { ?>
	<div class="cerita" onclick="window.location='/baca/<?= $cerita[$i]['id']?>'">
		<table style="width: 100%">
			<tr>
				<td>
					<b><?= $cerita[$i]['judul']?></b>
					<br>oleh <a href="/profil/aan"><?= $penulis[$i]['username']?></a>
				</td>
				<td style="display: flex; justify-content: flex-end">
					<button class="tmbl bg-bahaya" onclick="hapus(<?= $cerita[$i]['id'] ?>);event.stopPropagation()">&times;</button>
				</td>
			</tr>
		</table>
		<hr>
		<p><?= $cerita[$i]['deskripsi']?></p>
		<hr>
		<small><?= $cerita[$i]['genre'] ?></small>
	</div>
<?php } ?>

<div class="bg-popup" id="popup-hapus" onclick="batal('popup-hapus')">
	<div class="kartu-popup" onclick="event.stopPropagation()">
		<h3>Peringatan</h3>
		<form action="/hapusBaca" method="post">
			<?= csrf_field() ?>
			<input type="hidden" name="id_cerita" id="id_cerita">
			<p>Apa anda yakin ingin menghapus riwayat membaca ini?</p>
			<input type="submit" class="tmbl bg-bahaya" value="HAPUS">
			<span onclick="batal('popup-hapus')" class="tmbl" style="background: #888888;box-shadow: none; float: right">BATAL</span>
		</form>
	</div>
</div>

<script>
	function batal(id) {
		document.getElementById(id).style.display = 'none'
	}
	function hapus(id) {
		document.getElementById('popup-hapus').style.display = 'block'
		document.getElementById('id_cerita').value = id
	}
</script>
<?= $this->endSection() ?>