<?= $this->extend('user/template') ?>

<?= $this->section('konten') ?>
<h3>Buat Cerita</h3>
<form action="/tambahCeritaProses" method="post" class="tambah-cerita">
	<?= csrf_field() ?>
	<input type="text" name="judul" placeholder="Judul" max-length="100" title="Inputkan judul cerita di sini." required autofocus autocomplete="off"><br>
	<textarea name="deskripsi" rows="10" placeholder="deskripsi atau sinopsis cerita"></textarea><br>
	<input type="text" name="genre" autocomplete="off" title="Isi genre seperti fiksi ilmiah, komedi, dll. Pisahkan dengan tanda koma." placeholder="Genre"><br>
	<input type="submit" class="tmbl tmblbuat" value="BUAT">
</form>
<?= $this->endSection() ?>