<?= $this->extend('user/template') ?>

<?= $this->section('konten') ?>
<form action="/cari" method="post">
	<?= csrf_field() ?>
	<input type="text" class="cari cari-teks" placeholder="Cari..." name="cari" value="<?= $cari ?>" autocomplete="off" required min-length="3">
	<input type="submit" class="cari cari-tombol" value="Cari">
</form>
<br><br>

<?php for ($i=0; $i < count($cerita); $i++) { ?>
	<div class="cerita">
		<p><b><?= $cerita[$i]['judul']?></b> <br>oleh <a href="/profil/<?= $penulis[$i]['username']?>"><?= $penulis[$i]['username']?></a></p>
		<div onclick="window.location='/baca/<?= $cerita[$i]['id']?>'" class="linknya">
			<hr>
			<p><?= $cerita[$i]['deskripsi']?></p>
			<hr>
		</div>
		<?php $genre = explode(',', $cerita[$i]['genre']);?>
		<small>
			<?php for ($j=0; $j < count($genre); $j++) { ?>
				<form action="/cari" method="post" style="display: inline">
					<input type="submit" name="cari" value="<?= trim($genre[$j])?>">
				</form>
			<?php } ?>
		</small>
		<small style="float: right"><?= $suka[$i]?></small>
	</div>
<?php } ?>
<div style="display: flex; justify-content: space-evenly">
	<?php if($halaman != 0) { ?>
		<button class="tmbl" style="width: 30%" onclick="window.location='/list/<?= $halaman-1 ?>'"><--</button>
	<?php } ?>
	<?php if($halaman != $halaman_max) { ?>
		<button class="tmbl" style="width: 30%" onclick="window.location='/list/<?= $halaman+1 ?>'">--></button>
	<?php } ?>
</div>
<?= $this->endSection() ?>