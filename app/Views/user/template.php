<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Teks Only</title>
	<link rel="stylesheet" href="/style.css">
</head>
<body>
	<div class="nav">
		<input type="checkbox"" id="nav-check">
		<div class="nav-header">
			<div class="nav-title">
				Tx-Only
			</div>
		</div>
		<div class="nav-btn">
			<label for="nav-check">
				<span></span>
				<span></span>
				<span></span>
			</label>
		</div>

		<div class="nav-link">
			<a href="/">Home</a>
			<a href="/membaca">Dibaca</a>
			<a href="/ceritaku">Cerita Saya</a>
			<a href="/gantipw">Ganti Password</a>
			<a href="/keluar">&times;Keluar</a>
		</div>
	</div>
	<div class="container">
		<p><?= session()->getFlashdata('pesan') ?></p>
		<?= $this->renderSection('konten') ?>
	</div>
</body>
</html>