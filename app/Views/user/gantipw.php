<?= $this->extend('user/template') ?>

<?= $this->section('konten') ?>
<form action="/gantipw" method="post">
	<label for="password">Password baru:</label><br>
	<input type="password" required min-length="8" class="inputan" placeholder="password baru" id="password" autofocus name="password"><br><br>
	<label for="konfirmasi">Konfirmasi:</label><br>
	<input type="password" required min-length="8" class="inputan" placeholder="konfirmasi password" name="konfirmasi"><br><br>
	<input type="submit" value="GANTI" class="tmbl">
</form>
<?= $this->endSection() ?>