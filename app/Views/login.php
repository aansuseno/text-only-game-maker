<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Halaman Masuk</title>
	<style>
		* {
			margin: 0;
			font-family: sans-serif;
			box-sizing: border-box;
		}
		body {
			background: #d5ab9c;
			color: #007640;
		}
		.kotak-login {
			width: 100%;
			max-width: 400px;
			padding: 20px;
			margin: 120px auto;
		}

		input {
			width: 100%;
			padding: 7px;
			border-radius: 10px;
			margin-top: 20px;
			border: none;
		}

		.tombol-masuk {
			background: #4764dd;
			color: #ddd;
		}
	</style>
</head>
<body>
	<div class="kotak-login">
		<form action="/login" method="post">
			<?= csrf_field() ?>
			<center><h2>Masuk</h2></center>
			<br>
			<p><center><?= session()->getFlashdata('pesan') ?></center></p>
			<br>
			<input type="text" name="username" required autofocus autocomplete="off" placeholder="Username">
			<input type="password" id="pw" name="password" required placeholder="Password">
			<br><br>
			<p onclick="lihat()" style="cursor: pointer" id="lihat">lihat password</p>
			<input type="submit" value="MASUK" class="tombol-masuk">
			<br>
			<br>
			<a href="daftar">Belum punya akun?</a>
		</form>
	</div>

	<script>
		var terlihat = false
		function lihat() {
			if (terlihat == false) {
				document.getElementById('pw').type = 'text'
				document.getElementById('lihat').innerHTML = 'sembunyikan password'
				terlihat = true
			} else {
				document.getElementById('pw').type = 'password'
				document.getElementById('lihat').innerHTML = 'lihat password'
				terlihat = false
			}
		}
	</script>
</body>
</html>