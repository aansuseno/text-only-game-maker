<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php')) {
    require SYSTEMPATH . 'Config/Routes.php';
}

/*
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'Home::index', ['filter' => 'login']);
$routes->get('/keluar', 'Home::keluar', ['filter' => 'login']);
$routes->get('/ceritaku', 'Home::ceritaku', ['filter' => 'login']);
$routes->get('/tambah', 'Home::tambah', ['filter' => 'login']);
$routes->get('/buatHalaman/(:num)', 'Home::tambahHalaman/$1', ['filter' => 'login']);
$routes->get('/detailCeritaku/(:num)', 'Home::detailCeritaku/$1', ['filter' => 'login']);
$routes->post('/tambahCeritaProses', 'Home::tambahCeritaProses', ['filter' => 'login']);
$routes->post('/gantiIsiHalaman', 'Home::gantiIsiHalaman', ['filter' => 'login']);
$routes->post('/tayangkanCerita', 'Home::tayangkanCerita', ['filter' => 'login']);
$routes->post('/sembunyikanCerita', 'Home::sembunyikanCerita', ['filter' => 'login']);
$routes->post('/simpanAlur', 'Home::simpanAlur', ['filter' => 'login']);
$routes->post('/hapusAlur', 'Home::hapusAlur', ['filter' => 'login']);
$routes->post('/editAlur', 'Home::editAlur', ['filter' => 'login']);
$routes->get('/ambilPilihan/(:num)', 'Home::ambilPilihan/$1', ['filter' => 'login']);
$routes->get('/baca/(:num)', 'Membaca::index/$1', ['filter' => 'login']);
$routes->post('/baca/(:num)', 'Membaca::lanjutBaca/$1', ['filter' => 'login']);
$routes->post('/reset', 'Membaca::reset', ['filter' => 'login']);
$routes->get('/membaca', 'Membaca::dibaca', ['filter' => 'login']);
$routes->post('/hapusBaca', 'Membaca::hapusBaca', ['filter' => 'login']);
$routes->get('/suka/(:num)', 'Membaca::suka/$1', ['filter' => 'login']);
$routes->get('/list/(:num)', 'Home::index/$1', ['filter' => 'login']);
$routes->post('/cari', 'Home::index', ['filter' => 'login']);
$routes->get('/gantipw', 'Home::gantipw', ['filter' => 'login']);
$routes->post('/gantipw', 'Home::gantipwProses', ['filter' => 'login']);
$routes->post('/hapus-cerita', 'Home::hapusCerita', ['filter' => 'login']);
$routes->get('/profil/(:any)', 'Home::profil/$1', ['filter' => 'login']);
$routes->get('/editCeritaku/(:num)', 'Membaca::editCerita/$1', ['filter' => 'login']);
$routes->post('/editCeritaProses/(:num)', 'Membaca::editCeritaProses/$1', ['filter' => 'login']);

$routes->get('/login', 'Login::index', ['filter' => 'janganKeluar']);
$routes->get('/daftar', 'Login::daftar', ['filter' => 'janganKeluar']);
$routes->post('/daftar', 'Login::daftarProses', ['filter' => 'janganKeluar']);
$routes->post('/login', 'Login::loginProses', ['filter' => 'janganKeluar']);

$routes->get('/tes', function ()
{
	echo "tes jalan";
});

/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
    require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
